﻿using SemanticTest.Domain.Interfaces;
using System;
using System.Collections.Generic;

namespace SemanticTest.Implementation.Domain
{
    public class Calculator : ICalculator
    {
        public decimal ArithmeticMean(decimal[] numbers)
        {
            int i = 0;
            int lastPosition = numbers.Length - 1;
            decimal total = 0;
            while (i < lastPosition - i)
            {
                total = total + numbers[i] + numbers[lastPosition - i];
                i++;
            }
            if (i == lastPosition - i)
            {
                total = total + numbers[i];
                return total / (i * 2 + 1);
            }
            else
                return total / (i * 2);
        }

        public decimal[] FrequencyOfNumbers(decimal[] numbers)
        {
            decimal[] boxes = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            foreach(decimal number in numbers)
            {
                boxes[(int)(number / (decimal)10)] = ((boxes[(int)(number / (decimal)10)] * numbers.Length) + 1) / numbers.Length;
            }

            return boxes;
        }

        public decimal StandardDeviation(decimal[] numbers)
        {
            decimal arithmeticMean = ArithmeticMean(numbers);
            decimal variance = Variance(numbers, arithmeticMean);

            return (decimal)Math.Sqrt((double)variance);
        }

        private static decimal Variance(decimal[] numbers, decimal arithmeticMean)
        {
            int i = 0;
            int lastPosition = numbers.Length - 1;
            decimal total = 0;
            while (i < lastPosition - i)
            {
                total = total + ((numbers[i] - arithmeticMean) * (numbers[i] - arithmeticMean)) + ((numbers[lastPosition - i] - arithmeticMean) * (numbers[lastPosition - i] - arithmeticMean));
                i++;
            }
            if (i == lastPosition - i)
            {
                total = total + ((numbers[i] - arithmeticMean) * (numbers[i] - arithmeticMean));
            }
            return total / lastPosition;
        }
    }
}
