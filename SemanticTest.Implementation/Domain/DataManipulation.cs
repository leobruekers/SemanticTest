﻿using SemanticTest.Domain.Interfaces;
using System;
using System.IO;

namespace SemanticTest.Implementation.Domain
{
    public class DataManipulation : IDataManipulation
    {
        public decimal[] ConvertToArray(string numbers)
        {
            return Array.ConvertAll(numbers.Split(','), Decimal.Parse);
        }

        public string ReadFile(string path)
        {
            return File.ReadAllText(path);
        }

        public string ValidateIfFileExists(string fileName)
        {
            string path = "../../../Data/" + fileName + ".csv";
            if (File.Exists(path))
                return path;
            else return "";
        }
    }
}
