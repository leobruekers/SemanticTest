﻿using SemanticTest.Implementation.Domain;
using System;

namespace SemanticTest.Application
{
    class Program
    {
        static decimal[] numbers;

        static void Main(string[] args)
        {
            string fileName = "";
            do
            {
                Console.WriteLine("PLEASE, MAKE SURE THAT YOUR DATA IS IN THE PROJECT DATA FOLDER !");
                Console.WriteLine("Type the data file name or EXIT to exit:");
                fileName = Console.ReadLine();
                ExtractInformation(fileName);
                if(numbers != null)
                    RunAnalysis();
            } while (fileName != "EXIT");
        }

        static void ExtractInformation(string fileName)
        {
            DataManipulation dataManipulation = new DataManipulation();
            string completePath = dataManipulation.ValidateIfFileExists(fileName);
            if (completePath != "")
            {
                string fileInfo = dataManipulation.ReadFile(completePath);
                try
                {
                    numbers = dataManipulation.ConvertToArray(fileInfo);
                }
                catch(Exception ex)
                {
                    Console.WriteLine("Your data file is not on the right format.");
                    numbers = null;
                }
            }
            else
            {
                Console.WriteLine("Sorry, This file does not exists.");
            }
        }

        static void RunAnalysis()
        {
            Calculator calculator = new Calculator();
            Console.WriteLine("Arithmetic Mean: " + calculator.ArithmeticMean(numbers));
            Console.WriteLine("Standard Deviation: " + calculator.StandardDeviation(numbers));
            decimal[] distribution = calculator.FrequencyOfNumbers(numbers);
            for (int i = 0; i < distribution.Length; i++)
                Console.WriteLine(i * 10 + " to <" + (i + 1) * 10 + ": " + String.Format("{0:0.00}", distribution[i] * 100) + "%");
        }
    }
}
