﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SemanticTest.Domain.Interfaces
{
    public interface IDataManipulation
    {
        String ValidateIfFileExists(string fileName);
        String ReadFile(string path);
        Decimal[] ConvertToArray(string numbers);
    }
}
