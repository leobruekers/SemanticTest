﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SemanticTest.Domain.Interfaces
{
    public interface ICalculator
    {
        // To AritmeticMean and StandardDeviation was decided to use Decimal considering the accuracy
        Decimal ArithmeticMean(Decimal[] numbers);
        Decimal StandardDeviation(Decimal[] numbers);

        // To FrequencyOfNumbers was assumption that it have to return the percentage of occurrences
        decimal[] FrequencyOfNumbers(Decimal[] numbers);
    }
}
